/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author pratik
 */
@WebServlet(name = "registerCompany", urlPatterns = {"/registerCompany"})
public class registerCompany extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final String DBNAME = "StockExchange";
    private static final String DB_USERNAME = "chhipa";
    private static final String DB_PASSWORD = "";
    private static final String INSERT_COMPANY = "insert into company(name,web,total) values(?,?,?)";
    private static final String SELECT_COMPANY = "select ID from company where name = ?";
    private static final String INSERT_USER = "insert into user(login,password,name,age,cash,basecash) values(?,?,?,?,?,?)";
    private static final String SELECT_USER = "select ID from user where login = ?";
    private static final String INSERT_STOCK = "insert into stock(ticker,Company_ID) values(?,?)";
    private static final String GET_SID = "select ID from stock where ticker = ? and Company_ID = ?";
    private static final String OWN ="insert into ownership(price,volume, User_ID, Stock_ID) values(?,?,?,?)";
    private static final String GET_OID="select ID from Ownership where User_ID = ? and Stock_ID = ?";
    private static final String SELL ="insert into sell(price,volume, User_ID, Stock_ID,Ownership_ID) values(?,?,?,?,?)";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet registerCompany</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet registerCompany at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        String strName = request.getParameter("name").toString();
        String strWeb = request.getParameter("web").toString();
        String[] tkr;
        tkr = (String[])request.getParameterValues("tkr[]");
        String[] vol;
        vol = (String[])request.getParameterValues("vol[]");
        String[] price;
        price = (String[])request.getParameterValues("price[]");
        
        int size = vol.length;
        int sum=0;
        for(int i = 0; i<size; i++)
            sum+=Integer.parseInt(vol[i]);
        
        System.out.println(sum);
        System.out.println(size);
        Connection con;
        con = null;
        try {
            con = connect();
            PreparedStatement prepStmt = con.prepareStatement(INSERT_COMPANY);
            prepStmt.setString(1, strName);
            prepStmt.setString(2, strWeb);
            prepStmt.setInt(3,sum);
            int rs = prepStmt.executeUpdate();
            
        } catch(Exception e) {
                    try {
                        System.out.println("validateLogon: Error while validating password: "+e.getMessage());
                        throw e;
                    } catch (Exception ex) {
                        Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                    }
        } finally {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                    }
        }
        int id = 0;
        con = null;
        try {
            con = connect();
            PreparedStatement prepStmt = con.prepareStatement(SELECT_COMPANY);
            prepStmt.setString(1, strName);
            ResultSet rs = prepStmt.executeQuery();
            id = 0;
            while(rs.next()){
                id=rs.getInt(1);
            }
            System.out.println(id);
        } catch(Exception e) {
                    try {
                        System.out.println("validateLogon: Error while validating password: "+e.getMessage());
                        throw e;
                    } catch (Exception ex) {
                        Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                    }
        } finally {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                    }
        }
        
        con = null;
        try {
            con = connect();
            PreparedStatement prepStmt = con.prepareStatement(INSERT_USER);
            prepStmt.setString(1, "com_"+id+"");
            prepStmt.setString(2, "com_"+id+"");
            prepStmt.setString(3,strName);
            prepStmt.setInt(4,0);
            prepStmt.setInt(5,10000);
            prepStmt.setInt(6,10000);
            int rs = prepStmt.executeUpdate();
            
        } catch(Exception e) {
                    try {
                        System.out.println("validateLogon: Error while validating password: "+e.getMessage());
                        throw e;
                    } catch (Exception ex) {
                        Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                    }
        } finally {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                    }
        }
        int userid = 0;
        con = null;
        try {
            con = connect();
            PreparedStatement prepStmt = con.prepareStatement(SELECT_USER);
            prepStmt.setString(1, "com_"+id+"");
            ResultSet rs = prepStmt.executeQuery();
            while(rs.next()){
                userid=rs.getInt(1);
            }
            System.out.println("USERID"+userid);
        } catch(Exception e) {
                    try {
                        System.out.println("validateLogon: Error while validating password: "+e.getMessage());
                        throw e;
                    } catch (Exception ex) {
                        Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                    }
        } finally {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                    }
        }
        for(int j = 0; j<size; j++){
            con = null;
            try {
                con = connect();
                PreparedStatement prepStmt = con.prepareStatement(INSERT_STOCK);
                prepStmt.setString(1, tkr[j]);
                prepStmt.setInt(2, id);
                int rs = prepStmt.executeUpdate();

            } catch(Exception e) {
            try {
                System.out.println("validateLogon: Error while validating password: "+e.getMessage());
                throw e;
            } catch (Exception ex) {
                Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
            }
            } finally {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            int sid = 0;
            con = null;
            try {
                con = connect();
                PreparedStatement prepStmt = con.prepareStatement(GET_SID);
                prepStmt.setString(1, tkr[j]);
                prepStmt.setInt(2, id);
                ResultSet rs = prepStmt.executeQuery();
               
                while(rs.next()){
                    sid=rs.getInt(1);
                }
                System.out.println(sid);
            } catch(Exception e) {
                        try {
                            System.out.println("validateLogon: Error while validating password: "+e.getMessage());
                            throw e;
                        } catch (Exception ex) {
                            Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                        }
            } finally {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                        }
            }
            
            con = null;
            try {
                con = connect();
                PreparedStatement prepStmt = con.prepareStatement(OWN);
                prepStmt.setFloat(1, (Float.parseFloat(price[j])));
                prepStmt.setInt(2, Integer.parseInt(vol[j]));
                prepStmt.setInt(3, userid);
                prepStmt.setInt(4, sid);
                int rs = prepStmt.executeUpdate();

            } catch(Exception e) {
            try {
                System.out.println("validateLogon: Error while validating password: "+e.getMessage());
                throw e;
            } catch (Exception ex) {
                Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
            }
            } finally {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            
            int oid = 0;
            con = null;
            try {
                con = connect();
                PreparedStatement prepStmt = con.prepareStatement(GET_OID);
                prepStmt.setInt(1, userid);
                prepStmt.setInt(2, sid);
                ResultSet rs = prepStmt.executeQuery();
               
                while(rs.next()){
                    oid=rs.getInt(1);
                }
                System.out.println(oid);
            } catch(Exception e) {
                        try {
                            System.out.println("validateLogon: Error while validating password: "+e.getMessage());
                            throw e;
                        } catch (Exception ex) {
                            Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                        }
            } finally {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                        }
            }
            
            con = null;
            try {
                con = connect();
                PreparedStatement prepStmt = con.prepareStatement(SELL);
                prepStmt.setFloat(1, (Float.parseFloat(price[j])));
                prepStmt.setInt(2, Integer.parseInt(vol[j]));
                prepStmt.setInt(3, userid);
                prepStmt.setInt(4, sid);
                prepStmt.setInt(5, oid);
                int rs = prepStmt.executeUpdate();

            } catch(Exception e) {
            try {
                System.out.println("validateLogon: Error while validating password: "+e.getMessage());
                throw e;
            } catch (Exception ex) {
                Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
            }
            } finally {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(registerCompany.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        response.sendRedirect("index.jsp?page=0");
        
    }
    Connection connect() throws Exception
    {
        Connection con=null;
        try
        {
            String url = "jdbc:mysql://192.168.0.102:3306/"+DBNAME+"?user="+DB_USERNAME;
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url);
        } 
        catch (SQLException sqle) 
        {
            System.out.println("SQLException: Unable to open connection to db: "+sqle.getMessage());
            throw sqle;
        }
         catch(Exception e)
        {
            System.out.println("Exception: Unable to open connection to db: "+e.getMessage());
            throw e;
        }
        
        return con;
        
        
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
