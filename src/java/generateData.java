/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author pratik
 */
@WebServlet(name = "generateData", urlPatterns = {"/generateData"})
public class generateData extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private static final String DBNAME = "StockExchange";
    private static final String DB_USERNAME = "chhipa";
    private static final String DB_PASSWORD = "";
    private static final String DATA_QUERY = "select price, date from history where stock = ?";
    private static final String DATA_QUERY2 = "select price, date(date) from (SELECT History.id AS idhistory, History.stock, History.price,History.date, History.volume, Stock.ticker, Company.name FROM (select * FROM History order by History.date desc) as history, Stock, Company WHERE History.stock=Stock.id AND Stock.Company_ID=Company.id GROUP BY stock,date(History.date)) as A where A.stock = ?;";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet generateData</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet generateData at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        HttpSession session = request.getSession();
        int ID=(Integer)session.getAttribute("ID");
        int SID = (Integer)session.getAttribute("SID_GRAPH");
        System.out.println(ID);
         boolean isValid = false;
            Connection con;
            con = null;
            ResultSet rs;
            Vector v0 = new Vector();
            Vector v1 = new Vector();
            Vector v2 = new Vector();
            Vector v3 = new Vector();
            Vector v4 = new Vector();
            HashMap<Integer,String>m =null;
            try {
                con = connect();
                PreparedStatement prepStmt = con.prepareStatement(DATA_QUERY2);
                prepStmt.setInt(1, SID);
                rs = prepStmt.executeQuery();
                while(rs.next()){
                    v0.add(rs.getFloat(1));
                    v1.add(rs.getString(2));
                    String s = rs.getString(2);
                    //String a[] = s.split(" ");
                    String date[] = s.split("-");
                    //String time[] = a[1].split("-");
                    v2.add(Integer.parseInt(date[0]));
                    v3.add(Integer.parseInt(date[1]));
                    v4.add(Integer.parseInt(date[2]));
                }

            } catch(Exception e) {
                System.out.println("validateLogon: Error while fetching data: "+e.getMessage());
            } finally {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(showHistory.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            //ession.setAttribute("hashMap",map);
            session.setAttribute("data_price", v0);
            session.setAttribute("data_date", v1);
            session.setAttribute("data_year", v2);
            session.setAttribute("data_month", v3);
            session.setAttribute("data_day", v4);
            response.sendRedirect("plot.jsp");
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    Connection connect() throws Exception
    {
        Connection con=null;
        try
        {
            String url = "jdbc:mysql://192.168.0.102:3306/"+DBNAME+"?user="+DB_USERNAME;
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url);
        } 
        catch (SQLException sqle) 
        {
            System.out.println("SQLException: Unable to open connection to db: "+sqle.getMessage());
            throw sqle;
        }
         catch(Exception e)
        {
            System.out.println("Exception: Unable to open connection to db: "+e.getMessage());
            throw e;
        }
        
        return con;
        
        
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
