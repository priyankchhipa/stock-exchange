<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Vector"%>
<html>
  <head>
      <script type="text/javascript">
           function popup()
            {
               popupWindow = window.open('plot.jsp','name','width=200,height=200');
               popupWindow.focus();
            }
      </script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
            
            
            <%
            Vector price = (Vector)session.getAttribute("data_price");
            Vector date = (Vector)session.getAttribute("data_date");
            
            Vector year = (Vector)session.getAttribute("data_year");
            Vector month = (Vector)session.getAttribute("data_month");
            Vector day = (Vector)session.getAttribute("data_day");
            String tkr = (String)session.getAttribute("TKR_GRAPH");
            int size = price.size();
            %>
            
            
         
            var data = new google.visualization.DataTable();
            data.addColumn('date','Date');
            data.addColumn('number', 'Price');
            var a;
           <% int x=0;
            System.out.println(size);
            //out.print("data.addRows("+size+");");
            while(x<size){
           
            System.out.print(x+" "+size+" "+price.get(x)+" "+year.get(x)+" "+((Integer)month.get(x)-1)+" "+day.get(x)+"\n");
            //String[] parts = (date.get(x)).(" ");   
           %>
                //a = date.get(x).split(" ");
                //a = a[0].split("-");
                //data.addRow([new Date(Integer.parseInt(a[0]), Integer.parseInt(a[1]), Integer.parseInt(a[2])), <%= price.get(x) %> ]);
                data.addRow([new Date( <%= year.get(x) %> , <%= ((Integer)month.get(x)-1) %>, <%= day.get(x) %>), <%= price.get(x) %> ]);
                <% x++;
            }
            
            
            %>
            //        data.addColumn('date', 'Date');
    //data.addColumn('number', 'Quantity');
    //data.addRow([new Date(2011, 0, 1), 10])
    //data.addRow([new Date(2011, 1, 1), 15])
    //data.addRow([new Date(2011, 3, 1), 40])
    //data.addRow([new Date(2011, 6, 1), 50])
        var options = {
          title: 'Ticker : <%= tkr %>'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
  </body>
</html>