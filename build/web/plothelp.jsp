<%-- 
    Document   : plothelp
    Created on : Nov 7, 2012, 9:52:35 PM
    Author     : pratik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Plot Graph</title>
    </head>
    <body>
        <%
        Integer SID = Integer.parseInt(request.getParameter("sid"));
        String TKR = request.getParameter("tkr");
        session.setAttribute("SID_GRAPH", SID);
        session.setAttribute("TKR_GRAPH", TKR);
        response.sendRedirect("generateData");
        %>
    </body>
</html>
